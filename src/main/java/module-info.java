module ru.iraskhan.diplomnaya {
    requires javafx.controls;
    requires javafx.fxml;
    requires javafx.web;

    requires org.controlsfx.controls;
    requires com.dlsc.formsfx;
    requires net.synedra.validatorfx;
    requires org.kordamp.ikonli.javafx;
    requires org.kordamp.bootstrapfx.core;
    requires eu.hansolo.tilesfx;
    requires com.almasb.fxgl.all;
    requires static lombok;
    requires org.bouncycastle.provider;
    requires annotations;
    requires java.desktop;
    requires javafx.media;
    requires org.slf4j;

    opens ru.iraskhan.diplomnaya to javafx.fxml;
    exports ru.iraskhan.diplomnaya;
    exports ru.iraskhan.diplomnaya.method;
    opens ru.iraskhan.diplomnaya.method to javafx.fxml;
    exports ru.iraskhan.diplomnaya.controller;
    opens ru.iraskhan.diplomnaya.controller to javafx.fxml;
    exports ru.iraskhan.diplomnaya.error;
    opens ru.iraskhan.diplomnaya.error to javafx.fxml;
}