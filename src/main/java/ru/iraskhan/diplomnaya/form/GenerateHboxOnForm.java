package ru.iraskhan.diplomnaya.form;

import javafx.geometry.Pos;
import javafx.scene.control.ListView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import ru.iraskhan.diplomnaya.bubble.BubbleSpec;
import ru.iraskhan.diplomnaya.bubble.BubbledLabel;

import java.io.File;

public class GenerateHboxOnForm {
    public static HBox getHbox1(String msg,  ListView chatPane) {

        BubbledLabel bl6 = new BubbledLabel();

        bl6.setText(msg);
        bl6.setBackground(new Background(new BackgroundFill(Color.LIGHTGREEN,
                null, null)));
        HBox x = new HBox();
        x.setMaxWidth(chatPane.getWidth() - 20);
        x.setAlignment(Pos.TOP_RIGHT);
        bl6.setBubbleSpec(BubbleSpec.FACE_RIGHT_CENTER);
        x.getChildren().addAll(bl6);
        return x;
    }


    public static HBox getHbox2(String msg, String picture) {
        Image image = new Image(new File("images/" + picture.toLowerCase() + ".png").toURI().toString());
        ImageView profileImage = new ImageView(image);
        profileImage.setFitHeight(32);
        profileImage.setFitWidth(32);
        BubbledLabel bl6 = new BubbledLabel();

        bl6.setText(msg);

        bl6.setBackground(new Background(new BackgroundFill(Color.WHITE, null, null)));
        HBox x = new HBox();
        bl6.setBubbleSpec(BubbleSpec.FACE_LEFT_CENTER);
        x.getChildren().addAll(profileImage, bl6);
        return x;
    }
}
