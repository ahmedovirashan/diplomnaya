package ru.iraskhan.diplomnaya;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;
import lombok.Getter;
import ru.iraskhan.diplomnaya.controller.AbsController;
import ru.iraskhan.diplomnaya.controller.Controller;
import ru.iraskhan.diplomnaya.controller.FileController;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MainApplication extends Application {

    @Override
    public void start(Stage stage) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(MainApplication.class.getResource("views/main-view.fxml"));
        Scene scene = new Scene(fxmlLoader.load(), 520, 500);
        stage.setTitle("Шифрование слова");
        stage.setScene(scene);
        FileController fileController = fxmlLoader.getController();
        fileController.setStage(stage);
        stage.show();
    }

    public static void main(String[] args) {
        launch();
    }
}