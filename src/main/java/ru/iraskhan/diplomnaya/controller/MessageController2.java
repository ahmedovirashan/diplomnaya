package ru.iraskhan.diplomnaya.controller;

import ru.iraskhan.diplomnaya.MainApplication;

import java.util.List;

public class MessageController2 extends AbsController {
    @Override
    protected String getName() {
        return "B";
    }

    @Override
    public void getController() {
        List<Controller> controllerList = controllers;
        for (Controller controller : controllerList) {
            if (controller instanceof MessageController1) {
                // Используйте rsaController
                this.controller = (MessageController1) controller; // Если у вас только один экземпляр RsaController2, вы можете прервать цикл
            }
        }
    }
}
