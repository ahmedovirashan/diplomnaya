package ru.iraskhan.diplomnaya.controller;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import ru.iraskhan.diplomnaya.MainApplication;
import ru.iraskhan.diplomnaya.error.ErrorAndAlert;
import ru.iraskhan.diplomnaya.file.FileReaderWriter;
import ru.iraskhan.diplomnaya.form.GenerateHboxOnForm;
import ru.iraskhan.diplomnaya.method.ElGamal;
import ru.iraskhan.diplomnaya.method.MethodEncrypt;
import ru.iraskhan.diplomnaya.method.RSA;
import ru.iraskhan.diplomnaya.method.RsaOrElGamalEncrypt;
import ru.iraskhan.diplomnaya.window.Window;

import java.io.File;
import java.io.IOException;

public class FileController {
    @Getter
    @Setter
    private Stage stage;
    @FXML
    private TextArea messageBox;
    @FXML
    private ListView chatPane;
    @FXML
    private RsaOrElGamalEncrypt methodEncrypt = new RSA();
    @FXML
    private Label methodEncryptLbl;
    @FXML
    private Label timeEncryptLbl;
    @FXML
    private Label timeDecryptLbl;

    @Getter
    @Setter
    private boolean isCreated = false;

    private void generateKeyPair() throws Exception {
        methodEncrypt.generateKeyPair();
    }

    @FXML
    public void handleOpen(ActionEvent actionEvent) throws Exception {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Выберите файл");
        File file = fileChooser.showOpenDialog(stage);
        String text = FileReaderWriter.readFromFile(file);
        File key = new File("keys.txt");
        if (key.exists()) methodEncrypt = (RsaOrElGamalEncrypt) FileReaderWriter.readObject(key);
        else {
            ErrorAndAlert.getAlert("Нет файла с ключами");
            return;
        }
        assert methodEncrypt != null;
        long startTime = System.nanoTime();
        String decrypt = methodEncrypt.decrypt(text, methodEncrypt.getPrivateKey());
        long endTime = System.nanoTime();
        timeDecryptLbl.setText("Время дешифрования: " + (endTime - startTime) + " нс");
        chatPane.getItems().add(GenerateHboxOnForm.getHbox1(decrypt, chatPane));
    }

    @FXML
    public void handleSave() throws Exception {
        File key = new File("keys.txt");
        if (!key.exists()) {
            generateKeyPair();
            FileReaderWriter.writeObject(methodEncrypt, "keys.txt");
        } else methodEncrypt = (RsaOrElGamalEncrypt) FileReaderWriter.readObject(key);

        String text = messageBox.getText();
        chatPane.getItems().add(GenerateHboxOnForm.getHbox2(text, "default"));
        long startTime = System.nanoTime();
        String encrypt = methodEncrypt.encrypt(text, methodEncrypt.getPublicKey());
        long endTime = System.nanoTime();
        timeEncryptLbl.setText("Время шифрования: " + (endTime - startTime) + " нс");
        FileReaderWriter.writeInFile(encrypt, "text.txt");
        messageBox.clear();

    }

    @FXML
    public void handleClose(ActionEvent actionEvent) {
        Platform.exit();
    }

    @FXML
    public void handleRSA(ActionEvent actionEvent) {
        methodEncrypt = new RSA();
        methodEncryptLbl.setText("RSA");
    }

    @FXML
    public void handleElGamal(ActionEvent actionEvent) {
        methodEncrypt = new ElGamal();
        methodEncryptLbl.setText("Эль Гамаля");
    }


    @FXML
    public void openMessanger(ActionEvent actionEvent) throws IOException {
        Window.createNewWindowMessanger("views/main-view1.fxml", 520, 540);
        Window.createNewWindowMessanger("views/main-view2.fxml", 500, 520);
        stage.close();
    }

    public void actionSave(KeyEvent event) throws Exception {
        if (event.getCode() == KeyCode.ENTER) {
            handleSave();
        }
    }
}
