package ru.iraskhan.diplomnaya.controller;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import lombok.Getter;
import lombok.Setter;
import ru.iraskhan.diplomnaya.form.GenerateHboxOnForm;
import ru.iraskhan.diplomnaya.method.*;

import javax.crypto.SecretKey;
import java.io.File;
import java.io.IOException;
import java.security.KeyPair;
import java.util.ArrayList;
import java.util.List;


public abstract class AbsController implements Controller {

    @FXML
    private TextArea messageBox;
    @FXML
    private ListView chatPane;
    @FXML
    private ImageView userImageView = new ImageView(new File("images/" + getPicture().toLowerCase() + ".png").toURI().toString());
    @FXML
    private ListView chatPane2;
    @FXML
    protected Label methodEncryptLbl;
    @FXML
    private Label timeEncryptLbl;
    @FXML
    private Label timeDecryptLbl;

    @Getter
    private static List<Stage> stages = new ArrayList<>();
    @Getter
    protected static List<Controller> controllers = new ArrayList<>();
    protected static MethodEncrypt methodEncrypt = new RSA();
    @Getter
    private Object publicKey;
    protected AbsController controller;
    private Object privateKey;
    private boolean isGeneratedKey = false;
    private SecretKey secretKey;

    {
        getController();
    }

    @FXML
    private void sendMessage() throws Exception {
        String msg = messageBox.getText();
        if (!messageBox.getText().isEmpty()) {
            if (controller == null)
                getController();

            setImageLabel();
            HBox x = GenerateHboxOnForm.getHbox1(msg, chatPane);

            chatPane.getItems().add(x);

            String encrypt = null;
            if (!isGeneratedKey) {
                controller.generateKeyPair();
                generateKeyPair();
                isGeneratedKey = true;

                if (methodEncrypt instanceof EccOrDHEncrypt methodEncrypt1) {
                    secretKey = methodEncrypt1.generateSharedSecret(privateKey, controller.publicKey);
                    controller.secretKey = methodEncrypt1.generateSharedSecret(controller.privateKey, publicKey);
                }
            }

            long startTime = System.nanoTime();
            if (methodEncrypt instanceof EccOrDHEncrypt methodEncrypt1) {
                encrypt = methodEncrypt.encrypt(msg, secretKey);
            } else {
                encrypt = methodEncrypt.encrypt(msg, controller.publicKey);
            }
            long endTime = System.nanoTime();
            timeEncryptLbl.setText("Время шифрования: " + (endTime - startTime) + " нс");

            x = GenerateHboxOnForm.getHbox1(encrypt, chatPane2);
            chatPane2.getItems().add(x);

            controller.send(encrypt);
            messageBox.clear();
        }
    }


    @FXML
    private void sendMethod(KeyEvent event) throws Exception {
        if (event.getCode() == KeyCode.ENTER) {
            sendMessage();
        }
    }

    private void generateKeyPair() throws Exception {
        KeyPair keyPair = methodEncrypt.generateKeyPair();
        publicKey = keyPair.getPublic();
        privateKey = keyPair.getPrivate();
    }

    public void send(String msg) throws Exception {
        HBox x = GenerateHboxOnForm.getHbox2(getName() + ": " + msg, getPicture());
        chatPane2.getItems().add(x);

        String decrypt = null;

        long startTime = System.nanoTime();
        if (methodEncrypt instanceof EccOrDHEncrypt methodEncrypt1) {
            decrypt = methodEncrypt1.decrypt(msg, secretKey);
        } else {
            decrypt = methodEncrypt.decrypt(msg, privateKey);
        }
        long endTime = System.nanoTime();
        controller.timeDecryptLbl.setText("Время дешифрования: " + (endTime - startTime) + " нс");
        x = GenerateHboxOnForm.getHbox2(decrypt, getPicture());
        chatPane.getItems().add(x);
    }


    private String getPicture() {
        return "Default";
    }

    private void setImageLabel() throws IOException {
        this.userImageView.setImage(new Image(new File("images/default.png").toURI().toString()));
    }

    protected abstract String getName();

    @Override
    public abstract void getController();

    @FXML
    private void closeApplication() {
        Platform.exit();
        System.exit(0);
    }

    @FXML
    private void methodDiffieHellman() {
        methodEncrypt = new DiffieHellman();
        methodEncryptLbl.setText("Диффи-Хелмана");
        controller.
                methodEncryptLbl.setText("Диффи-Хелмана");
        isGeneratedKey = false;
    }

    @FXML
    private void methodRsa() {
        methodEncrypt = new RSA();
        methodEncryptLbl.setText("RSA");
        controller.
                methodEncryptLbl.setText("RSA");
        isGeneratedKey = false;
    }

    @FXML
    private void methodElGamale() {
        methodEncrypt = new ElGamal();
        methodEncryptLbl.setText("Эль Гамаля");
        controller.
                methodEncryptLbl.setText("Эль Гамаля");
        isGeneratedKey = false;
    }

    @FXML
    private void methodEcc() {
        methodEncrypt = new ECC();
        methodEncryptLbl.setText("ECC");
        controller.
                methodEncryptLbl.setText("ECC");
        isGeneratedKey = false;
    }

}
