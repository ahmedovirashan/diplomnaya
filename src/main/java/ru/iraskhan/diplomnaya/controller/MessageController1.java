package ru.iraskhan.diplomnaya.controller;

import ru.iraskhan.diplomnaya.MainApplication;

import java.util.List;

public class MessageController1 extends AbsController {
    @Override
    protected String getName() {
        return "A";
    }

    @Override
    public void getController() {
        List<Controller> controllerList = controllers;
        for (Controller controller : controllerList) {
            if (controller instanceof MessageController2) {
                // Используйте rsaController
                this.controller = (MessageController2) controller; // Если у вас только один экземпляр RsaController2, вы можете прервать цикл
            }
        }
    }
}
