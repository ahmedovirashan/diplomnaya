package ru.iraskhan.diplomnaya.error;

import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;

public class ErrorAndAlert {
    public static void getAlert(String s) {
        ButtonType yesButton = new ButtonType("Да");
        ButtonType noButton = new ButtonType("Нет");
        ButtonType cancelButton = new ButtonType("Отмена");

        Alert alert = new Alert(Alert.AlertType.CONFIRMATION,  s, yesButton, noButton, cancelButton);
        alert.setTitle("Ошибка");
        alert.setHeaderText(null);
        alert.showAndWait();

        alert.showAndWait().ifPresent(response -> {
            if (response == yesButton) {
                System.out.println("Пользователь выбрал: Да");
            } else if (response == noButton) {
                System.out.println("Пользователь выбрал: Нет");
            } else if (response == cancelButton) {
                System.out.println("Пользователь выбрал: Отмена");
            }
        });
    }

    public static void getError(String s) {
        ButtonType goodButton = new ButtonType("Хорошо");
        Alert alert = new Alert(Alert.AlertType.ERROR, s, goodButton);
        alert.setTitle("Ошибка");
        alert.setHeaderText(null);
        alert.showAndWait();
    }
}
