package ru.iraskhan.diplomnaya.file;

import org.jetbrains.annotations.NotNull;
import ru.iraskhan.diplomnaya.error.ErrorAndAlert;

import java.awt.image.AreaAveragingScaleFilter;
import java.io.*;
import java.util.Arrays;

public class FileReaderWriter {
    public static String readFromFile(File file) throws RuntimeException {
        try (FileReader reader = new FileReader(file)) {
            char[] buff = new char[1];
            StringBuilder sb = new StringBuilder();
            while (reader.read(buff) != -1) {
                sb.append(buff);
            }
            return sb.toString();
        } catch (Exception e) {
            ErrorAndAlert.getAlert("Ошибка при чтении из файла");
        }
        return null;
    }

    public static void writeInFile(String text, String fileTitle) {
        String title = fileTitle;
        if(title == null)
            title = "output.txt";
        try(FileWriter fileWriter = new FileWriter(title)){
            fileWriter.write(text);
        } catch (IOException e) {
            ErrorAndAlert.getAlert("Ошибка при записи в файл");
        }
    }

    public static Object readObject(File file) {
        try(ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file))){
            return ois.readObject();
        }catch (Exception e){
            ErrorAndAlert.getAlert("Ошибка при чтении объекта из фала");
        }
        return null;
    }

    public static void writeObject(Object o, String filTitle) {
        String title = filTitle;
        if(title == null)
            title = "key.txt";
        try(ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(title))){
                oos.writeObject(o);
        } catch (Exception e) {
            ErrorAndAlert.getAlert("Ошибка при записи объекта в файл");
        }
    }
}
