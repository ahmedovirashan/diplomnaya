package ru.iraskhan.diplomnaya.method;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import javax.crypto.Cipher;
import javax.crypto.KeyAgreement;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.security.*;
import java.util.Base64;

public class DiffieHellman extends EccOrDHEncrypt {

    {
        Security.addProvider(new BouncyCastleProvider());
    }

    // Генерация пары ключей Diffie-Hellman
    @Override
    public KeyPair generateKeyPair() throws Exception {
        KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("DH", "BC");
        keyPairGenerator.initialize(2048);

        KeyPair keyPair = keyPairGenerator.generateKeyPair();
        privateKey = keyPair.getPrivate();
        publicKey = keyPair.getPublic();
        return keyPair;
    }

    // Генерация общего секретного ключа
    @NotNull
    @Contract("_, _ -> new")
    public SecretKey generateSharedSecret(Object privateKey1, Object publicKey) throws Exception {
        PublicKey publicKey1 = (PublicKey) publicKey;
        PrivateKey privateKey = (PrivateKey) privateKey1;
        KeyAgreement keyAgreement = KeyAgreement.getInstance("DH", "BC");
        keyAgreement.init(privateKey);
        keyAgreement.doPhase(publicKey1, true);
        byte[] sharedSecret = keyAgreement.generateSecret();
        secretKey = new SecretKeySpec(sharedSecret, 0, 16, "AES");
        return secretKey;
    }

    // Шифрование с использованием AES
    @Override
    public String encrypt(String encryptText, Object secretKey) throws Exception {
        SecretKey secretKey1 = (SecretKey) secretKey;
        Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding", "BC");
        cipher.init(Cipher.ENCRYPT_MODE, secretKey1);
        byte[] encryptedBytes = cipher.doFinal(encryptText.getBytes());
        return Base64.getEncoder().encodeToString(encryptedBytes);
    }

    // Расшифровка с использованием AES
    @Override
    public String decrypt(String encryptedText, Object secretKey) throws Exception {
        SecretKey secretKey1 = (SecretKey) secretKey;
        Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding", "BC");
        cipher.init(Cipher.DECRYPT_MODE, secretKey1);
        byte[] decryptedBytes = cipher.doFinal(Base64.getDecoder().decode(encryptedText));
        return new String(decryptedBytes);
    }

    public String main(String args) {
        try {
            // Генерация ключевых пар для двух сторон
            KeyPair keyPairAlice = generateKeyPair();
            KeyPair keyPairBob = generateKeyPair();

            // Обмен публичными ключами
            PublicKey publicKeyAlice = keyPairAlice.getPublic();
            PrivateKey privateKeyAlice = keyPairAlice.getPrivate();
            PublicKey publicKeyBob = keyPairBob.getPublic();
            PrivateKey privateKeyBob = keyPairBob.getPrivate();

            // Генерация общего секретного ключа
            SecretKey sharedSecretAlice = generateSharedSecret(privateKeyAlice, publicKeyBob);
            SecretKey sharedSecretBob = generateSharedSecret(privateKeyBob, publicKeyAlice);

            // Проверка, что оба секретных ключа совпадают
            System.out.println("Shared secret (Alice): " + Base64.getEncoder().encodeToString(sharedSecretAlice.getEncoded()));
            System.out.println("Shared secret (Bob): " + Base64.getEncoder().encodeToString(sharedSecretBob.getEncoded()));

            // Сообщение для шифрования
            String originalMessage = "Hello, Diffie-Hellman!";
            System.out.println("Original Message: " + originalMessage);

            // Шифрование
            String encryptedMessage = encrypt(originalMessage, sharedSecretAlice);
            System.out.println("Encrypted Message: " + encryptedMessage);

            // Расшифровка
            String decryptedMessage = decrypt(encryptedMessage, sharedSecretBob);
            System.out.println("Decrypted Message: " + decryptedMessage);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return args;
    }
}