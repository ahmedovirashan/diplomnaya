package ru.iraskhan.diplomnaya.method;

import lombok.Getter;
import lombok.Setter;

import javax.crypto.SecretKey;
import java.security.PrivateKey;

public abstract class EccOrDHEncrypt extends MethodEncrypt {
    @Getter @Setter
    protected SecretKey secretKey;

    public abstract SecretKey generateSharedSecret(Object privateKey, Object publicKey) throws Exception;
}
