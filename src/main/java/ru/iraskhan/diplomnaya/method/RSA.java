package ru.iraskhan.diplomnaya.method;

import javafx.scene.control.Alert;
import ru.iraskhan.diplomnaya.error.ErrorAndAlert;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.security.*;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

public class RSA extends RsaOrElGamalEncrypt{

    // Генерация пары ключей RSA
    @Override
    public KeyPair generateKeyPair() {
        KeyPairGenerator keyGen = null;
        try {
            keyGen = KeyPairGenerator.getInstance("RSA");
        } catch (NoSuchAlgorithmException ignore) {
            ErrorAndAlert.getAlert("generateRSAKeyPair method alert");
        }
        assert keyGen != null;
        keyGen.initialize(2048);

        KeyPair keyPair = keyGen.generateKeyPair();
        privateKey = keyPair.getPrivate();
        publicKey = keyPair.getPublic();
        return keyPair;
    }

    // Шифрование строки
    @Override
    public String encrypt(String plainText, Object publicKey) {
        PublicKey publicKey1 = (PublicKey) publicKey;
        try {
            Cipher cipher = Cipher.getInstance("RSA");
            cipher.init(Cipher.ENCRYPT_MODE, publicKey1);
            byte[] encryptedBytes = cipher.doFinal(plainText.getBytes());
            return Base64.getEncoder().encodeToString(encryptedBytes);
        } catch (NoSuchPaddingException | IllegalBlockSizeException | NoSuchAlgorithmException | BadPaddingException |
                 InvalidKeyException ignore) {
            ErrorAndAlert.getAlert("encrypt method alert");
        }
        return null;
    }

    // Расшифровка строки
    @Override
    public String decrypt(String encryptedText, Object privateKey) {
        PrivateKey privateKey1 = (PrivateKey) privateKey;
        Cipher cipher = null;
        try {
            cipher = Cipher.getInstance("RSA");
            cipher.init(Cipher.DECRYPT_MODE, privateKey1);
            byte[] decryptedBytes = cipher.doFinal(Base64.getDecoder().decode(encryptedText));
            return new String(decryptedBytes);
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | IllegalBlockSizeException | BadPaddingException |
                 InvalidKeyException ignore) {
            ErrorAndAlert.getAlert("decrypt method alert");
        }
        return null;
    }
}
