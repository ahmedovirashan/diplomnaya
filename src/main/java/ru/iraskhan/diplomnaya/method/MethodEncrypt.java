package ru.iraskhan.diplomnaya.method;

import lombok.Getter;
import lombok.Setter;

import javax.crypto.SecretKey;
import java.security.KeyPair;
import java.security.PrivateKey;
import java.security.PublicKey;

public abstract class MethodEncrypt {
    @Getter @Setter
    protected PublicKey publicKey;
    @Getter @Setter
    protected PrivateKey privateKey;

    public abstract KeyPair generateKeyPair() throws Exception;

    public abstract String encrypt(String plainText, Object publicKey) throws Exception;

    public abstract String decrypt(String encryptedText, Object privateKey) throws Exception;

}
