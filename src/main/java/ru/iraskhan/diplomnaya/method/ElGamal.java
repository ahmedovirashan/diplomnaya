package ru.iraskhan.diplomnaya.method;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.jce.spec.ElGamalParameterSpec;

import java.security.*;
import javax.crypto.Cipher;
import java.security.KeyPair;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Security;
import java.util.Base64;

public class ElGamal extends RsaOrElGamalEncrypt {

    static {
        Security.addProvider(new BouncyCastleProvider());
    }

    // Генерация пары ключей ElGamal
    @Override
    public KeyPair generateKeyPair() throws Exception {
        KeyPairGenerator keyGen = KeyPairGenerator.getInstance("ElGamal", "BC");
        AlgorithmParameterGenerator paramGen = AlgorithmParameterGenerator.getInstance("ElGamal", "BC");
        paramGen.init(256);
        AlgorithmParameters params = paramGen.generateParameters();
        ElGamalParameterSpec elParams = params.getParameterSpec(ElGamalParameterSpec.class);
        keyGen.initialize(elParams, new SecureRandom());

        KeyPair keyPair = keyGen.generateKeyPair();
        privateKey = keyPair.getPrivate();
        publicKey = keyPair.getPublic();
        return keyPair;
    }

    // Шифрование строки
    @Override
    public String encrypt(String plainText, Object publicKey) throws Exception {
        PublicKey publicKey1 = (PublicKey) publicKey;
        Cipher cipher = Cipher.getInstance("ElGamal/None/PKCS1Padding", "BC");
        cipher.init(Cipher.ENCRYPT_MODE, publicKey1);
        byte[] encryptedBytes = cipher.doFinal(plainText.getBytes());
        return Base64.getEncoder().encodeToString(encryptedBytes);
    }

    // Расшифровка строки
    @Override
    public String decrypt(String encryptedText, Object privateKey) throws Exception {
        PrivateKey privateKey1 = (PrivateKey) privateKey;
        Cipher cipher = Cipher.getInstance("ElGamal/None/PKCS1Padding", "BC");
        cipher.init(Cipher.DECRYPT_MODE, privateKey1);
        byte[] decryptedBytes = cipher.doFinal(Base64.getDecoder().decode(encryptedText));
        return new String(decryptedBytes);
    }

    public String main(String args) {
        try {
            // Генерация ключей
            KeyPair keyPair = generateKeyPair();
            PublicKey publicKey = keyPair.getPublic();
            PrivateKey privateKey = keyPair.getPrivate();

            // Оригинальное сообщение
            String originalMessage = "Hello, ElGamal!";
            System.out.println("Original Message: " + originalMessage);

            // Шифрование
            String encryptedMessage = encrypt(originalMessage, publicKey);
            System.out.println("Encrypted Message: " + encryptedMessage);

            // Расшифровка
            String decryptedMessage = decrypt(encryptedMessage, privateKey);
            System.out.println("Decrypted Message: " + decryptedMessage);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return args;
    }
}