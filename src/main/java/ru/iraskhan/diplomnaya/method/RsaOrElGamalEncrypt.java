package ru.iraskhan.diplomnaya.method;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.security.PrivateKey;
import java.security.PublicKey;

public abstract class RsaOrElGamalEncrypt extends MethodEncrypt implements Serializable {
    @Getter
    @Setter
    protected PublicKey publicKey;
    @Getter
    @Setter
    protected PrivateKey privateKey;
}
