package ru.iraskhan.diplomnaya.window;

import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import ru.iraskhan.diplomnaya.MainApplication;
import ru.iraskhan.diplomnaya.controller.AbsController;
import ru.iraskhan.diplomnaya.controller.FileController;
import ru.iraskhan.diplomnaya.error.ErrorAndAlert;

import java.io.IOException;

public class Window {
    private static boolean isCreate = false;

    public static void createNewWindowMessanger(String file, int v, int v1) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(MainApplication.class.getResource(file));
        Scene scene = new Scene(fxmlLoader.load(), v, v1);
        Stage stage = new Stage();
        stage.setTitle("Шифрование слова");
        stage.setScene(scene);
        AbsController.getStages().add(stage);
        AbsController.getControllers().add(fxmlLoader.getController());
        stage.setOnHiding(new EventHandler<WindowEvent>() {

            @Override
            public void handle(WindowEvent event) {
                Platform.runLater(new Runnable() {

                    @Override
                    public void run() {
                        if (!AbsController.getStages().isEmpty()) {
                            for (Stage st : AbsController.getStages()) {
                                st.close();
                            }
                            try {
                                if (!isCreate)
                                    Window.createNewWindowFile();
                            } catch (IOException e) {
                                ErrorAndAlert.getAlert("Ошибка при открытии Файла");
                            }
                            AbsController.getStages().clear();
                        }
                    }
                });
            }
        });
        stage.show();
    }

    public static void createNewWindowFile() throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(MainApplication.class.getResource("views/main-view.fxml"));
        Scene scene = new Scene(fxmlLoader.load(), 500, 520);
        Stage stage = new Stage();
        stage.setTitle("Шифрование слова");
        stage.setScene(scene);
        isCreate = true;
        stage.setOnHiding(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent windowEvent) {
                Platform.runLater(new Runnable() {
                    @Override
                    public void run(){
                        isCreate = false;
                    }
                });
            }
        });
        FileController fileController = fxmlLoader.getController();
        fileController.setStage(stage);
        stage.show();
    }
}
